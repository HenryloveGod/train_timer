/*
 * tmethod.c
 *
 *  Created on: 2017年5月2日
 *      Author: dl
 */


#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#include "tmethod.h"
#include "functions.h"
#include "debug.h"
#include "string.h"
#include "safe.h"
#include "cJSON.h"

/*
 * 秒
 * */
void seconds_sleep(unsigned seconds)
{
    struct timeval tv;
    tv.tv_sec=seconds;
    tv.tv_usec=0;
    int err;
    do{
    	err=select(0,NULL,NULL,NULL,&tv);
    }while(err<0 && errno==EINTR);

}

/*
 * 毫秒
 * */
void milliseconds_sleep(unsigned long mSec)
{
    struct timeval tv;
    tv.tv_sec=mSec/1000;
    tv.tv_usec=(mSec%1000)*1000;
    int err;
    do{
       err=select(0,NULL,NULL,NULL,&tv);
    }while(err<0 && errno==EINTR);
}


/*
 * eotu_wifidog fucntion
 * */
int check_eotu_wifidog(pargs_struct_t *p){

	FILE *f;
	int flg=0;
	char buf[1024];

	f = popen("/bin/ps","r");
	while(fgets(buf,1024,f)){
		if(strstr(buf,"eotu_wifidog")){
			flg=1;
			debug(LOG_INFO,"eotu_wifidog got, its OK! ");
			break;
		}
	}

	int rc=0;
	if(flg == 0){
		debug(LOG_INFO,"eotu_wifidog not run,will restart");
		if(safe_fork() ==0 ){
			system("eotu_wifidog");
	        exit(1);
		}
	}
	return rc;
}
void *monitor_eotu_wifidog(pargs_struct_t *p)
{
	int rc;
	while(1){
		rc = check_eotu_wifidog(p);
		seconds_sleep(p->interval);
	}
	return NULL;
}

/*
 * reboot fucntion
 * */
int reboot(pargs_struct_t *p)
{
	debug(LOG_INFO,"Attention! Reboot now!");
	return system("reboot");
}
void *monitor_reboot(pargs_struct_t *p)
{
	int rc;
	while(1){
		seconds_sleep(p->interval);
		rc = reboot(p);
		debug(LOG_INFO,"reboot result [%s]",rc);
	}
}
/*
 * ntptime fucntion
 * 与0.asia.pool.ntp.org對时
 * */
int ntptime(pargs_struct_t *p)
{
	char *cmd ="ntpd -n -q -p 0.asia.pool.ntp.org";
	return system(cmd);
}
void *monitor_ntptime(pargs_struct_t *p)
{
	int rc;
	while(1){
		rc = ntptime(p);
		debug(LOG_INFO,"ntptime result[%d]",rc);
		seconds_sleep(p->interval);
	}
	return NULL;
}

/*
 * eotu_sync fucntion
 * 与eotu 服务器同步
 * rsync -vzrtopg --delete --password-file=/etc/rsync.pwd  113.195.207.216::www /mnt/eotusd/
 * */
int eotu_sync(pargs_struct_t *p)
{
	char *dest,*src;
	dest=cJSON_GetObjectItem(p->paras,"dest")->valuestring;
	src =cJSON_GetObjectItem(p->paras,"src")->valuestring;

	char *cmd ="rsync -vzrtopg --delete --password-file=/etc/rsync.pwd  113.195.207.216::www /mnt/eotusd/";
	safe_asprintf(&cmd,"rsync -vzrtopg --delete --password-file=/etc/rsync.pwd %s %s",src,dest);
	return system(cmd);
}
void *task_eotu_sync(pargs_struct_t *p)
{
	int rc;
	while(1){
		rc = eotu_sync(p);
		debug(LOG_INFO,"task_eotu_sync result[%d]",rc);
		seconds_sleep(p->interval);
	}
	return NULL;
}





/*
 *
 * */
int log_size_monitor(pargs_struct_t *p)
{
	char *pr,*cl;
	pr=cJSON_GetObjectItem(p->paras,"process")->valuestring;
	cl =cJSON_GetObjectItem(p->paras,"clients")->valuestring;

	file_pn_struct_t *prst,*clst;
	int rc;

	prst = get_file_path(pr);
	clst = get_file_path(cl);

	rc = delete_extra_debug_file(prst);
	rc = delete_extra_debug_file(clst);

	return rc;

}



void task_log_size_monitor(pargs_struct_t *p)
{

	int rc;
	while(1){
		rc = log_size_monitor(p);
		debug(LOG_INFO,"task_log_size_monitor result[%d]",rc);
		seconds_sleep(p->interval);
	}

}






/*
 * 根据配置，返回对应函数
 * */
void *((*get_fuctions(char *method))(pargs_struct_t *))
{
	if(strcmp(method,"ntptime")== 0 )
		return monitor_ntptime;
	else if(strcmp(method,"eotu_wifidog")== 0 )
		return monitor_eotu_wifidog;
	else if(strcmp(method,"reboot")== 0 )
		return monitor_reboot;
	else if(strcmp(method,"syn")== 0 )
		return task_eotu_sync;
	else if(strcmp(method,"log_size_monitor")== 0 )
		return task_log_size_monitor;
	else
		return NULL;

}

/*
 * 读取 /etc/eotu/timemachine.conf.json配置数据
 * */
task_struct_t *get_task_list()
{
	task_struct_t *task_l,*tmp_task,*pre_task;
	FILE *f;
	char *fstr;
	cJSON *tmp_j,*option_j;

	//读取配置文件
	fstr = read_file_to_string(OPT_PATH);
	if(!fstr){
		debug(LOG_ERR,"read options fail");
		return NULL;
	}
	//转化为json格式
	option_j = cJSON_Parse(fstr);
	if(!option_j){
		debug(LOG_ERR,"cJSON_Parse options to json fail");
		return NULL;
	}

	//赋值到task_struct_t 中
	tmp_j = option_j->child;
	task_l = get_task_header();
	tmp_task=task_l;
	pre_task=task_l;

	while(tmp_j){

		// set functions
		if(!tmp_j->string){
			debug(LOG_ERR,"json options function NULL");
			return task_l;
		}
		tmp_task->name = safe_strdup(tmp_j->string);
		tmp_task->func = get_fuctions(tmp_j->string);

		if(!tmp_task->func){
			debug(LOG_ERR,"not found function %s",tmp_j->string);
			if(tmp_task == pre_task)
				return NULL;
			else
				pre_task->next=NULL;
			return task_l;
		}

		//set interval time
		if(!cJSON_GetObjectItem(tmp_j,"interval")){
			debug(LOG_ERR,"json options function %s no interval",tmp_j->string);
			return task_l;
		}
		tmp_task->pargs = safe_malloc(sizeof(pargs_struct_t));
		tmp_task->pargs->interval = cJSON_GetObjectItem(tmp_j,"interval")->valuedouble;
		tmp_task->pargs->paras = tmp_j;

		//next
		if(NULL == tmp_task->next && tmp_j->next){
			tmp_task->next = malloc(sizeof(task_struct_t));
			bzero(tmp_task->next ,sizeof(task_struct_t));
			pre_task = tmp_task;
			tmp_task=tmp_task->next;
		}
		tmp_j = tmp_j->next;
	}

	return task_l;
}


