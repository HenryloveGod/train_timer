/*
 * config.c
 *
 *  Created on: 2017年5月4日
 *      Author: dl
 */


#include "config.h"
#include "safe.h"
#include "string.h"

static task_struct_t *task_header;


task_struct_t *get_task_header()
{
	return task_header;
}


void task_header_init()
{
	task_header = safe_malloc(sizeof(task_struct_t));
	bzero(task_header,sizeof(task_struct_t));

}

