/*
 ============================================================================
 Name        : timemachine.c
 Author      : Henry
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */


#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/prctl.h>

#include "debug.h"
#include "tmethod.h"  //include config.h
#include "functions.h"
#include "safe.h"
#include "cJSON.h"
#include "server_sock.h"





/** Exits cleanly after cleaning up the firewall.
 *  Use this function anytime you need to exit after firewall initialization.
 *  @param s Integer that is really a boolean, true means voluntary exit, 0 means error.
 */
void
termination_handler(int s)
{
	int tpid;
	int status;
	task_struct_t *task_header;
	pid_t w;

	task_header = get_task_header();

	while(task_header){
		//debug(LOG_INFO,"准备杀死 %s %u  ",task_header->name,task_header->pid );
		if(task_header->pid > 0 ){
	        do {
	        	debug(LOG_INFO," KILL %s %u ",task_header->name,task_header->pid );
	            kill(task_header->pid, SIGTERM);
	            sleep(2);
	            kill(task_header->pid, SIGKILL);
	            w = waitpid(task_header->pid, &status, WUNTRACED | WCONTINUED);
	            if (w == -1) { perror("waitpid"); exit(EXIT_FAILURE); }

	            if (WIFEXITED(status)) {
	                printf("exited, status=%d\n", WEXITSTATUS(status));
	            } else if (WIFSIGNALED(status)) {
	                printf("killed by signal %d\n", WTERMSIG(status));
	            } else if (WIFSTOPPED(status)) {
	                printf("stopped by signal %d\n", WSTOPSIG(status));
	            } else if (WIFCONTINUED(status)) {
	                printf("continued\n");
	            }
	        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
		}

		task_header = task_header->next;
	}

    debug(LOG_NOTICE, "Exiting...");
    exit(s == 0 ? 1 : 0);
}


/**@internal
 * @brief Handles SIGCHLD signals to avoid zombie processes
 *
 * When a child process exits, it causes a SIGCHLD to be sent to the
 * process. This handler catches it and reaps the child process so it
 * can exit. Otherwise we'd get zombie processes.
 */
void
sigchld_handler(int s)
{
    int status;
    pid_t rc;
    debug(LOG_DEBUG, "Handler for SIGCHLD called. Trying to reap a child");
    rc = waitpid(-1, &status, WNOHANG);

}

/** @internal
 * Registers all the signal handlers
 */
static void
init_signals(void)
{
    struct sigaction sa;

    debug(LOG_DEBUG, "Initializing signal handlers");

    //SIGCHLD子进程退出告知父进程
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }


    /*SIGPIPE
     * 已经关闭的socket调用两次write, 第二次将会生成SIGPIPE信号，默认结束进程
     * sigaction,信号排队堵塞
     */
    sa.sa_handler = SIG_IGN;  //忽略该信号
    if (sigaction(SIGPIPE, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    sa.sa_handler = termination_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART; //由此信号中断的系统调用会自动重启

    /* Trap SIGTERM 正常退出 阻塞处理 The shell command kill generates SIGTERM by default. */
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    /* Trap SIGQUIT Ctr+\*/
    if (sigaction(SIGQUIT, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    /* Trap SIGINT 中断键（Ctr+C）*/
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }
}


int do_main()
{
	int result;
	task_struct_t *task_l,*tmp_task;
	pid_t tmp_pid;

	//signal初始化
	init_signals();
	//task 初始化
	task_header_init();

	//获取配置文件，转为task_struct_t
	task_l = get_task_list();
	tmp_task = task_l;

	//创建进程运行各定时任务
	while(tmp_task){
		tmp_pid = fork();
		if(0==tmp_pid){
			tmp_task->pid = getpid();
			prctl(PR_SET_NAME, (unsigned long)tmp_task->name);
			debug(LOG_INFO,">>>>>>>>>>>>GET PID %s %u",tmp_task->name,tmp_task->pid );
			tmp_task->func(tmp_task->pargs);
		}

		tmp_task = tmp_task->next;

	}
	/*主进程任务监听socket*/
	thread_wdctl("/tmp/eotu_tm.sock");

	return 0;

}

/*用fork，后台执行*/
int main(int argc,char **argv) {

// fork a daemon
	pid_t pid;

	int daemon=0;
	if(argc >=2)
		daemon = atoi(argv[1]);

	if(daemon ==1){
		pid=fork();
		if(pid == 0){
			setsid();
			do_main();
		}else{
			printf("daemon start\r\n");
			exit(0);
		}
	}else
		do_main();
}
