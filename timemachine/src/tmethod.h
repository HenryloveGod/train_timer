/*
 * tmethod.h
 *
 *  Created on: 2017年5月2日
 *      Author: dl
 */

#ifndef TMETHOD_H_
#define TMETHOD_H_



#endif /* TMETHOD_H_ */

#include "config.h"


void seconds_sleep(unsigned seconds);
void milliseconds_sleep(unsigned long mSec);
void microseconds_sleep(unsigned long uSec);


void *monitor_eotu_wifidog(pargs_struct_t *p);
void *monitor_ntptime(pargs_struct_t *p);
void *monitor_reboot(pargs_struct_t *p);

task_struct_t *get_task_list();
