/*
 * functions.h
 *
 *  Created on: 2017年4月22日
 *      Author: dl
 */



/*
 * 当前时间
 * */
char * current_time();

int execute(const char *cmd_line, int quiet);

char *read_file_to_string(char *filepath);
