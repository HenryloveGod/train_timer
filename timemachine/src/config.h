/*
 * config.h
 *
 *  Created on: 2017年5月4日
 *      Author: dl
 */

#ifndef CONFIG_H_
#define CONFIG_H_



#endif /* CONFIG_H_ */

#include <sys/types.h>
#include "cJSON.h"

#define OPT_PATH "/etc/eotu/timemachine.conf.json"


//参数
typedef struct pargs_struct{
	double interval; //等待秒数
	cJSON *paras;  //函数参数
	struct pargs_struct *next;
}pargs_struct_t;


//函数链表

typedef struct task_struct{
	void *(*func)(pargs_struct_t *p);
	char *name;
	pid_t pid;
	pargs_struct_t *pargs;
	struct task_struct *next;
}task_struct_t;


void task_header_init();
task_struct_t *get_task_header();


