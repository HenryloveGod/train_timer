/*
 ============================================================================
 Name        : timemachine.c
 Author      : Henry
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */


#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include<sys/wait.h>

#include "debug.h"
#include "functions.h"
#include "safe.h"
#include "tmethod.h"
#include "cJSON.h"


#define OPT_PATH "/etc/eotu/timemachine.conf.json"

typedef struct pid_struct{
	pid_t pid;
	char *pidname;
	struct pid_struct *next;
}pid_struct_t;

//函数链表

typedef struct func_struct{
	void *(*func)(pargs_struct_t *p);
	char *name;
	pargs_struct_t *pargs;
	struct func_struct *next;
}func_struct_t;

static pid_struct_t *pid_task,*pid_s;;




void *((*get_fuctions(char *method))(pargs_struct_t *))
{
	if(strcmp(method,"ntptime")== 0 )
		return monitor_ntptime;
	else if(strcmp(method,"eotu_wifidog")== 0 )
		return monitor_eotu_wifidog;
	else if(strcmp(method,"reboot")== 0 )
		return monitor_reboot;
	else
		return NULL;


}

/*
 * 读取 /etc/eotu/timemachine.conf.json配置数据
 * */
func_struct_t *get_task_para()
{
	func_struct_t *p,*tp,*pre_p;

	FILE *f;
	char *fstr;

	fstr = read_file_to_string(OPT_PATH);

	if(!fstr){
		debug(LOG_ERR,"read options fail");
		return NULL;
	}
	cJSON *tmp_j,*option_j;

	option_j = cJSON_Parse(fstr);
	if(!option_j){
		debug(LOG_ERR,"cJSON_Parse options to json fail");
		return NULL;
	}

	tmp_j = option_j->child;

	p = malloc(sizeof(func_struct_t));
	tp = p;
	pre_p = NULL;
	while(tmp_j){

		// set functions
		if(!tmp_j->string){
			debug(LOG_ERR,"json options function NULL");
			return p;
		}
		tp->func = get_fuctions(tmp_j->string);
		tp->name = tmp_j->string;
		if(!tp->func){
			debug(LOG_ERR,"not found function %s",tmp_j->string);
			if(tp == pre_p)
				return NULL;
			else
				pre_p->next=NULL;

			return p;
		}

		//set interval time

		if(!cJSON_GetObjectItem(tmp_j,"interval")){
			debug(LOG_ERR,"json options function %s no interval",tmp_j->string);
			return p;
		}
		tp->pargs = safe_malloc(sizeof(pargs_struct_t));
		tp->pargs->interval = cJSON_GetObjectItem(tmp_j,"interval")->valuedouble;

		//next
		if(NULL == tp->next && tmp_j->next){
			tp->next = malloc(sizeof(func_struct_t));
			pre_p = tp;
			tp=tp->next;
		}
		tmp_j = tmp_j->next;
	}

	return p;
}



/** Exits cleanly after cleaning up the firewall.
 *  Use this function anytime you need to exit after firewall initialization.
 *  @param s Integer that is really a boolean, true means voluntary exit, 0 means error.
 */
void
termination_handler(int s)
{
	int tpid;
	pid_s = pid_task;
	int status;

	pid_t w;

	while(pid_s){
		if(pid_s->pid >0 && strcmp(pid_s->pidname,"eotu_wifidog") != 0){
			printf("\r\n 杀死 %u \r\n\r\n",pid_s->pid );

	        do {
	            kill(pid_s->pid, SIGTERM);
	            sleep(2);
	            kill(pid_s->pid, SIGKILL);
	            w = waitpid(pid_s->pid, &status, WUNTRACED | WCONTINUED);
	            if (w == -1) { perror("waitpid"); exit(EXIT_FAILURE); }

	            if (WIFEXITED(status)) {
	                printf("exited, status=%d\n", WEXITSTATUS(status));
	            } else if (WIFSIGNALED(status)) {
	                printf("killed by signal %d\n", WTERMSIG(status));
	            } else if (WIFSTOPPED(status)) {
	                printf("stopped by signal %d\n", WSTOPSIG(status));
	            } else if (WIFCONTINUED(status)) {
	                printf("continued\n");
	            }
	        } while (!WIFEXITED(status) && !WIFSIGNALED(status));

		}

		pid_s = pid_s->next;
	}

    debug(LOG_NOTICE, "Exiting...");
    exit(s == 0 ? 1 : 0);
}




/**@internal
 * @brief Handles SIGCHLD signals to avoid zombie processes
 *
 * When a child process exits, it causes a SIGCHLD to be sent to the
 * process. This handler catches it and reaps the child process so it
 * can exit. Otherwise we'd get zombie processes.
 */
void
sigchld_handler(int s)
{
    int status;
    pid_t rc;
    //debug(LOG_DEBUG, "Handler for SIGCHLD called. Trying to reap a child");
    rc = waitpid(-1, &status, WNOHANG);

}

/** @internal
 * Registers all the signal handlers
 */
static void
init_signals(void)
{
    struct sigaction sa;

    debug(LOG_DEBUG, "Initializing signal handlers");

    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    /* Trap SIGPIPE */
    /* This is done so that when libhttpd does a socket operation on
     * a disconnected socket (i.e.: Broken Pipes) we catch the signal
     * and do nothing. The alternative is to exit. SIGPIPE are harmless
     * if not desirable.
     */
    sa.sa_handler = SIG_IGN;
    if (sigaction(SIGPIPE, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    sa.sa_handler = termination_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    /* Trap SIGTERM */
    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    /* Trap SIGQUIT */
    if (sigaction(SIGQUIT, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }

    /* Trap SIGINT */
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        debug(LOG_ERR, "sigaction(): %s", strerror(errno));
        exit(1);
    }
}




int do_main()
{
	init_signals();

	int result;

	func_struct_t *p;
	p = get_task_para();

	/*根据设置，创建线程*/
	pid_task = safe_malloc(sizeof(pid_struct_t));
	pid_s = pid_task;

	while(p){
		pid_s->pid = fork();


		printf("\r\n 获取到PID %u \r\n\r\n",pid_s->pid );


		if(0==pid_s->pid){
			p->func(p->pargs);
			pid_s->pidname = p->name;
		}else
			p = p->next;

		if(!pid_s->next)
			pid_s->next = safe_malloc(sizeof(pid_struct_t));

		pid_s = pid_s->next;

	}

	/*保持主线程活着*/
	int i=0;
	while(1){
		printf("\r\n%d\r\n",i++);
		seconds_sleep(60);
	}
	return 0;

}

/*用fork，后台执行*/
int main(int argc,char **argv) {

// fork a daemon
	pid_t pid;

	int daemon=0;
	if(argc >=2)
		daemon = atoi(argv[1]);

	if(daemon ==1){
		pid=fork();
		if(pid == 0){
			setsid();
			do_main();
		}else{
			printf("daemon start\r\n");
			exit(0);
		}
	}else
		do_main();
}
