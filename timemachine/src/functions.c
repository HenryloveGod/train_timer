/*
 * functions.c
 *
 *  Created on: 2017年4月22日
 *      Author: dl
 */

#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#include "safe.h"
#include "debug.h"

#define WD_SHELL_PATH "/usr/bin"








/*
 * 当前时间
 * */
char * current_time()
{
	time_t nowtime;
	struct tm *local;
	static char time_mark[80];

	nowtime = time(NULL);
	local=localtime(&nowtime);

	strftime(time_mark,80,"%Y%m%d_%H%M%S",local);
	return time_mark;
}

/**/
int execute(const char *cmd_line)
{
    int pid, status, rc;

    const char *new_argv[4];
    new_argv[0] = WD_SHELL_PATH;
    new_argv[1] = "-c";
    new_argv[2] = cmd_line;
    new_argv[3] = NULL;

    pid = safe_fork();
    if (pid == 0) {             /* for the child process:         */
        if (execvp(WD_SHELL_PATH, (char *const *)new_argv) == -1) { /* execute the command  */
            debug(LOG_ERR, "execvp(): %s", strerror(errno));
        }else
        	debug(LOG_INFO,"execute [%s] OK",cmd_line);
        exit(1);
    }

    /* for the parent:      */
    debug(10, "Waiting for PID %d to exit", pid);
    rc = waitpid(pid, &status, 0);
    if (-1 == rc) {
        debug(LOG_ERR, "waitpid() failed (%s)", strerror(errno));
        return 1; /* waitpid failed. */
    }
    if (WIFEXITED(status)) {
        return (WEXITSTATUS(status));
    } else {
        debug(LOG_DEBUG, "Child may have been killed.");
        return 1;
    }
}



/*
*	字符串内部替换
*/
char *str_replace(char *orig, char *rep, char *with) {
    char *result; // the return string
    char *ins;    // the next insert point
    char *tmp;    // varies
    int len_rep;  // length of rep
    int len_with; // length of with
    int len_front; // distance between rep and end of last rep
    int count;    // number of replacements

    if (!orig)
        return NULL;
    if (!rep)
        rep = "";
    len_rep = strlen(rep);
    if (!with)
        with = "";
    len_with = strlen(with);

    ins = orig;
    for (count = 0; NULL != (tmp = strstr(ins, rep)) ; ++count) {
        ins = tmp + len_rep;
    }

    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    while (count--) {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    strcpy(tmp, orig);
    return result;
}




/*
*	读取整个文件到字符串中
*/
char *read_file_to_string(char *filepath)
{
    FILE * pFile;
    long lSize;
    char * buffer;
    size_t result;

    /* 若要一个byte不漏地读入整个文件，只能采用二进制方式打开 */
    pFile = fopen (filepath, "rb" );
    if (pFile==NULL)
    {
        fputs ("File error",stderr);
        return NULL;
    }

    /* 获取文件大小 */
    fseek (pFile , 0 , SEEK_END);
    lSize = ftell (pFile);
    rewind (pFile);

    /* 分配内存存储整个文件 */
    buffer = (char*) malloc (sizeof(char)*lSize);
    if (buffer == NULL)
    {
        fputs ("Memory error",stderr);
        return NULL;
    }

    /* 将文件拷贝到buffer中 */
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize)
    {
        fputs ("Reading error",stderr);
       	return NULL;
    }
	fclose(pFile);
	return buffer;

}



file_pn_struct_t *get_file_path(char *logfile)
{
	file_pn_struct_t *pn;
	pn = safe_malloc(sizeof(file_pn_struct_t));

	char *sfile = safe_strdup(logfile);
	char *token="";
	char *direct="/";

	token =strsep(&sfile,"/");
	while(sfile){
		if(strcmp(token,"")!=0)
			safe_asprintf(&direct,"%s%s/",direct,token);
		token = strsep(&sfile,"/");
	}
	pn->path=direct;
	pn->name = token;
	return pn;
}


int create_debug_file_dir(file_pn_struct_t *pn)
{

	//判断文件路径是否存在
	if(access(pn->path,0)!=0  ) {
	      if(mkdir(pn->path, 0755)==-1){
	    	  debug(LOG_ERR," mkdir [%s]fail",pn->path);
	    	  return -1;
	      }
	      debug(LOG_NOTICE,"mkdir log path :%s",pn->path);
	}
	return 0;
}


int delete_extra_debug_file(file_pn_struct_t *pn)
{
	DIR *dirptr = NULL;
	struct dirent *entry;
	int qty;
	char *tmpfile;

	//用ls方法省去排序的计算 ：：））
	char *cmd;
	safe_asprintf(&cmd,"ls -lt %s ",pn->path);
	qty=0;
	FILE *pp = popen(cmd, "r"); //建立管道
    char tmp[1024]; //设置一个合适的长度，以存储每一行输出
    while (fgets(tmp, sizeof(tmp), pp) != NULL) {

    	tmpfile = strstr((char *)tmp,pn->name);
    	if(!tmpfile)
    		continue;
    	tmpfile = str_replace(tmpfile,"\n","");
		qty++;
		if(qty>10){
				safe_asprintf(&tmpfile,"%s%s",pn->path,tmpfile);
				debug(LOG_NOTICE,"Log Clean delete %s\r\n", tmpfile);
				remove(tmpfile);
				free(tmpfile);
				qty--;
		}
    }

return 0;

}

/**********************************************************/

FILE *get_file_fd(char *logfile)
{
	FILE *fd=NULL;
	int newflg=0;

    if(strstr(logfile,"process"))
    	fd = prfd;
    else if(strstr(logfile,"clients"))
    	fd = clfd;
    else
    	return NULL;
	/*******************
	 * 判断文件大小，超过2 000 000,2MB,重新创建
	 * *************************/
    struct stat statbuf;
    int size;
    if(stat(logfile,&statbuf) ==0){
    	size=statbuf.st_size;
    	if(size >20000){
    		char *newname;
    		safe_asprintf(&newname,"%s_%s",logfile,current_time());
    		rename(logfile,newname);
    		newflg =1;
    	}
    }

    //新创建时用到
    if(!fd)
    	newflg=1;

    if(newflg ==1){
    	if(fd>0)
    		fclose(fd);
    	//日志路径，在debug_init中创建
		fd=fopen(logfile,"a+");
		if(fd <0){
			debug(LOG_ERR,"open log [%s] fail,change logpath",logfile);
			fd=fopen("/tmp/process.log","a+");
			if(fd <0)
				return NULL;
		}
    }

    return fd;
}





//日志文件初始化
int debug_file_init(char *processlog,char *clientslog)
{
	int fd;
	debug(LOG_NOTICE,"debug init");
	eotudebugflg=1;
	prpath = processlog;
	clpath = clientslog;

	file_pn_struct_t *pn = get_file_path(processlog);
	file_pn_struct_t *cn = get_file_path(clientslog);

	//判断日志路径是否存在，否则创建
	create_debug_file_dir(pn);
	create_debug_file_dir(cn);

	//检测日志文件数量，过量则删除
	delete_extra_debug_file(pn);
	delete_extra_debug_file(cn);

	/*
	//检测当前文件大小，过大重命名，再新建
	get_file_fd(processlog);
	get_file_fd(clientslog);
	*/

	return 0;

}

int debug_file_close()
{
	if(prfd)
		fclose(prfd);
	if(clfd)
		fclose(clfd);
	return 0;
}











