/*
 * server.h
 *
 *  Created on: 2017年5月5日
 *      Author: dl
 */

#ifndef SERVER_SOCK_H_
#define SERVER_SOCK_H_
#endif /* SERVER_SOCK_H_ */


void termination_handler(int s); //define on main.c


static void *thread_wdctl_handler(void *arg);

/*
 * arg必须为一个可以监听的sock文件路径名
*/
void thread_wdctl(void *arg);


